function imgStruct = LoadImgStruct(imgPath,imgType)
split=strsplit(imgPath,'/');
procFold=strjoin(split(1:end-2),'/');
procFold=[procFold,'/Processed/'];

img=vuOpenImage(imgPath);

imgStruct.Spc=img.Spc;
imgStruct.TR=img.Parms.repetition_time;
%imgStruct.Origin=img.Origin;
imgStruct.Origin=[0,0,0];

%Only keep the main part of the image (not all of the black surrounding space
if size(img.Data,4)>1
    img.Data2=img.Data;
    img.Data=img.Data(:,:,:,1,1);
end
ind=find(img.Data==0);
[x,y,z]=ind2sub(size(img.Data),ind);
xu=unique(x); yu=unique(y); zu=unique(z);
mid=size(img.Data)/2;
xlow=xu(xu<mid(1)); ylow=yu(yu<mid(2)); zlow=zu(zu<mid(3));
xlow=[0;xlow]; ylow=[0;ylow]; zlow=[0;zlow]; 
xlow=xlow(end); ylow=ylow(end); zlow=zlow(end);
xhigh=xu(xu>mid(1)); yhigh=yu(yu>mid(2)); zhigh=zu(zu>mid(3));
xhigh=[xhigh;size(img.Data,1)]; yhigh=[yhigh;size(img.Data,2)]; zhigh=[zhigh;size(img.Data,3)];
xhigh=xhigh(1); yhigh=yhigh(1); zhigh=zhigh(1);
slice=[size(img.Data,2)*size(img.Data,3),size(img.Data,1)*size(img.Data,3),size(img.Data,1)*size(img.Data,2)];
zer=[1,1;1,1;1,1];
bounds=[1,size(img.Data,1);1,size(img.Data,2); 1,size(img.Data,3)];

xx=xlow;
while zer(1,1)==1 && xx>bounds(1,1)
    ind2=find(x==xx);
    if size(ind2,1)==slice(1)
        bounds(1,1)=xx;
        zer(1,1)=0;
    else
        xx=xx-1;
    end
end

xx=xhigh;
while zer(1,2)==1 && xx<bounds(1,2)
    ind2=find(x==xx);
    if size(ind2,1)==slice(1)
        bounds(1,2)=xx;
        zer(1,2)=0;
    else
        xx=xx+1;
    end
end

yy=ylow;
while zer(1,1)==1 && yy>bounds(2,1)
    ind2=find(y==yy);
    if size(ind2,1)==slice(2)
        bounds(2,1)=yy;
        zer(2,1)=0;
    else
        yy=yy-1;
    end
end

yy=yhigh;
while zer(2,2)==1 && yy<bounds(2,2)
    ind2=find(y==yy);
    if size(ind2,1)==slice(2)
        bounds(2,2)=yy;
        zer(2,2)=0;
    else
        yy=yy+1;
    end
end

zz=zlow;
while zer(3,1)==1 && zz>bounds(3,1)
    ind2=find(x==zz);
    if size(ind2,1)==slice(3)
        bounds(3,1)=zz;
        zer(3,1)=0;
    else
        zz=zz-1;
    end
end

zz=zhigh;
while zer(3,2)==1 && zz<bounds(3,2)
    ind2=find(z==zz);
    if size(ind2,1)==slice(3)
        bounds(3,2)=zz;
        zer(3,2)=0;
    else
        zz=zz+1;
    end
end

bounds=reshape(bounds',[1,6]);

if isfield(img, 'Data2')
    img.Data=img.Data2;
    img=rmfield(img,'Data2');
end

imgStruct.Data=img.Data(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),:,:);
imgStruct.Dims=size(imgStruct.Data,1:3);

switch imgType
    case 'DWI'
        temp=readmatrix([procFold 'BValVec_DWI.txt']);
        imgStruct.bDWI=max(temp(:,1));
    case 'DTI'
        temp=readmatrix([procFold 'BValVec_DTI.txt']);
        imgStruct.DTIb=max(temp(:,1));
        imgStruct.DTIdir=temp(:,2:4);

        imgStruct.Data=cat(4,imgStruct.Data(:,:,:,1:end-2),imgStruct.Data(:,:,:,end));
        
end

img=loadParRec(imgPath);
imgStruct.TE=img.imgdef.echo_time.uniq';
imgStruct.FA=img.imgdef.image_flip_angle_in_degrees.uniq;
imgStruct.Orientation=img.imgdef.slice_orientation_TRA_SAG_COR.uniq;

end