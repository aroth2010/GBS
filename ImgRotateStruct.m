function imgRot=ImgRotateStruct(imgOrig)
imgRot=imgOrig;

switch imgOrig.Orientation
    case 1 %Axial acquisition - no further steps necessary
    case 2 %Sagittal acquisition
        imgRot.Data=permute(imgOrig.Data, [2,3,1,4,5]);
%         imgRot.Data=flip(imgRot.Data,1);
%         imgRot.Data=permute(imgOrig.Data, [3,2,1,4,5]);
%         imgRot.Data=flip(imgRot.Data,3);
        imgRot.Spc(1)=imgOrig.Spc(3);
        imgRot.Spc(2)=imgOrig.Spc(2);
        imgRot.Spc(3)=imgOrig.Spc(1);
        imgRot.Origin=[imgOrig.Origin(3),imgOrig.Origin(2),imgOrig.Origin(1)];
        imgRot.Orientation=1;
        imgRot.Dims=size(imgRot.Data,1:3);
    case 3 %Coronal acquisition
        imgRot.Data=permute(imgOrig.Data, [3,2,1,4,5]);
        imgRot.Data=flip(imgRot.Data,2);
%         imgRot.Data=permute(imgOrig.Data, [2,3,1,4,5]);
%         imgRot.Data=flip(imgRot.Data,3);
        imgRot.Spc(1)=imgOrig.Spc(2);
        imgRot.Spc(2)=imgOrig.Spc(3);
        imgRot.Spc(3)=imgOrig.Spc(1);
        imgRot.Origin=[imgOrig.Origin(2),imgOrig.Origin(3),imgOrig.Origin(1)];
        imgRot.Orientation=1;
        imgRot.Dims=size(imgRot.Data,1:3);
end


end