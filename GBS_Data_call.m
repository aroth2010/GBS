%GBS Analysis Main/Call
close all; clear; clc;
analysisGoal='analysis'; %'prep','analysis'
Registration=1;

pat={'dev_01','dev_02','001','002','003'};
fLoc{1}='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Scan Development/';
fLoc{2}='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Data/';

addpath('/Users/alisonroth/Documents/Code/MATLAB/Data_Load_Save/');
addpath('/Users/alisonroth/Documents/Code/MATLAB/vuTools/');

out=NaN(size(pat,2),11); %Patient, Scan, ADC, AD, RD, MD, FA, MTR, MTR B1 corrected, MTsat
x=1;

for p=1:size(pat,2)
    for s=1:2
        patient=pat{p};
        scan=['Scan ' num2str(s)];

        if strcmp(patient(1),'d')
            FolderLoc='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Scan Development/';
        elseif strcmp(patient(1),'0')
            FolderLoc='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Data/';
        else
            disp(['Warning patient not recognized: ' pat{p}])
        end
        if exist([FolderLoc 'GBS_' pat{p} '/' scan '/'])
            disp([patient ' ' scan]);
            if strcmp(analysisGoal,'prep')
%                 if ~exist([FolderLoc 'GBS_' pat{p} '/' scan '/Processed/GBS_' pat{p} '_Rotated.mat'])
                    GBS_Data_Prep(patient,FolderLoc,scan)
%                 end
        
                GBS_Data_Processing(patient,FolderLoc,scan,Registration)
            elseif strcmp(analysisGoal,'analysis')
                temp=GBS_Data_Analysis(patient,FolderLoc,scan);
                out(x,:)=[p,s,temp];
                x=x+1;
            end
        end
    end
end