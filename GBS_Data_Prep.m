%This is the first step in the GBS data processing pipeline
%
% Prior to running this script, move duplicated/unnecessary (i.e., Survey)
% scans to Raw/Not Analyzed or Raw/Analyze later folders
% 
% This script renames all remaining scans in the Raw folder, assembles all
%files into a .mat file (GBS_X_original.mat), and rotates all images to a common
%orientation (GBS_X_rotated.mat)

% close all; clear; clc
% patient='dev_01'; %'dev_02' or for patients: '001' 'phantom_02'
% scan='Scan 1';
% FolderLoc='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Scan Development/';
% FolderLoc='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Data/';

function GBS_Data_Prep(patient,FolderLoc,scan)
ptname=['GBS_' patient];
rawstart=[FolderLoc ptname '/' scan '/Raw/' ptname];
procfilestart=[FolderLoc ptname '/' scan '/Processed/' ptname];

addpath('/Users/alisonroth/Documents/Code/MATLAB/vuTools')
addpath('/Users/alisonroth/Documents/Code/MATLAB/Data_Load_Save/')

rawFolder=[FolderLoc 'GBS_' patient '/' scan '/Raw/'];
scans=dir([rawFolder '*.par']);
imgType={'MTR', 'AFI', 'DWI', 'PDw', 'T1w', 'T2w', 'EDDY', 'DTI'};%, 'NerveVIEW'};
imgTypeNames={'MT','B1_AFI','DWI','PDw','T1w','T2w','DTI_EDDY','DTI'};%,'NerveVIEW'};
spcMin=10;

for s=1:size(scans,1)
    it=1;
    while isempty(strfind(scans(s).name,imgType{it}))
        it=it+1;
    end
    disp(imgType{it});

    %Rename ParRec files:
    fstart=[FolderLoc ptname '/' scan '/Raw/' scans(s).name];
    fend=[rawstart '_' imgType{it} '.PAR'];
    if ~strcmp(fstart,fend)
        movefile(fstart,fend);
        movefile([FolderLoc ptname '/' scan '/Raw/' scans(s).name(1:end-3) 'REC'],[rawstart '_' imgType{it} '.REC']);
    end

    tempOrig=LoadImgStruct([rawstart '_' imgType{it} '.PAR'], imgType{it});
    tempRot=ImgRotateStruct(tempOrig);

    %Find min voxel sizes
    spcMin=min([spcMin,tempRot.Spc]);

    %Rename .Data to file types?
    switch imgType{it}
        case 'MTR'
            if size(tempOrig.Data,4)>1
                try
                    tempOrig.sMTON=tempOrig.Data(:,:,:,1,2); %Should average over fourth dimension
                    tempOrig.sMTOFF=tempOrig.Data(:,:,:,1,1); %Should average over fourth dimension
                    tempRot.sMTON=tempRot.Data(:,:,:,1,2); %Should average over fourth dimension
                    tempRot.sMTOFF=tempRot.Data(:,:,:,1,1); %Should average over fourth dimensio
                catch
                    tempOrig.sMTON=tempOrig.Data(:,:,:,2); %Should average over fourth dimension
                    tempOrig.sMTOFF=tempOrig.Data(:,:,:,1); %Should average over fourth dimension
                    tempRot.sMTON=tempRot.Data(:,:,:,2); %Should average over fourth dimension
                    tempRot.sMTOFF=tempRot.Data(:,:,:,1); %Should average over fourth dimensio
                end
                tempOrig=rmfield(tempOrig, 'Data');
                tempRot=rmfield(tempRot, 'Data');
            else
                disp('Warning: Not enough data from MTR scans.')
            end
        case 'DTI'
            tempOrig.b0=tempOrig.Data(:,:,:,end);
            tempRot.b0=tempRot.Data(:,:,:,end);
    end

%     save_nii(nii,[procfilestart imgType{it} '.nii']);

    eval([imgTypeNames{it} 'Orig = tempOrig;']);
    eval([imgTypeNames{it} ' = tempRot;']);
    
    if s==1
        save([procfilestart '_Original.mat'],[imgTypeNames{it} 'Orig'],'-v7.3');
        save([procfilestart '_Rotated.mat'],imgTypeNames{it},'-v7.3');
    else
        save([procfilestart '_Original.mat'],[imgTypeNames{it} 'Orig'],'-append','-v7.3');
        save([procfilestart '_Rotated.mat'],imgTypeNames{it},'-append','-v7.3');
    end

    clearvars -except patient scan FolderLoc rawFolder scans ptname rawstart procfilestart imgType imgTypeNames spcMin
end

%Resample all rotated images to common cubic voxel size
load([procfilestart '_Rotated.mat']);

varnames=who;
varnames=intersect(varnames,imgTypeNames);

for i=1:size(varnames,1)
    switch varnames{i}
        case 'MT'
            scale=MT.Spc/spcMin.*size(MT.sMTOFF,1:3);
            MT.sMTOFF=imresize3(MT.sMTOFF,scale);
            MT.sMTON=imresize3(MT.sMTON,scale);
            MT.Spc=[spcMin,spcMin,spcMin];
            MT.Dims=size(MT.sMTON,1:3);
            nii=make_nii(MT.sMTOFF,MT.Spc,MT.Origin);
        case 'B1_AFI'
            scale=B1_AFI.Spc/spcMin.*size(B1_AFI.Data,1:3);
            temp=zeros(ceil([scale,size(B1_AFI.Data,4)]));
            for d=1:size(B1_AFI.Data,4)
                temp(:,:,:,d)=imresize3(B1_AFI.Data(:,:,:,d),scale);
            end
            B1_AFI.Data=temp;
            B1_AFI.Spc=[spcMin,spcMin,spcMin];
            B1_AFI.Dims=size(B1_AFI.Data,1:3);
            nii=make_nii(B1_AFI.Data,B1_AFI.Spc,B1_AFI.Origin);
        case 'DWI'
            scale=DWI.Spc/spcMin.*size(DWI.Data,1:3);
            temp=zeros(ceil([scale,size(DWI.Data,4)]));
            for d=1:size(DWI.Data,4)
                temp(:,:,:,d)=imresize3(DWI.Data(:,:,:,d),scale);
            end
            DWI.Data=temp;
            DWI.Spc=[spcMin,spcMin,spcMin];
            DWI.Dims=size(DWI.Data,1:3);
            nii=make_nii(DWI.Data,DWI.Spc,DWI.Origin);
        case 'PDw'
            scale=PDw.Spc/spcMin.*size(PDw.Data,1:3);
            PDw.Data=imresize3(PDw.Data,scale);
            PDw.Spc=[spcMin,spcMin,spcMin];
            PDw.Dims=size(PDw.Data,1:3);
            nii=make_nii(PDw.Data,PDw.Spc,PDw.Origin);
        case 'T1w'
            scale=T1w.Spc/spcMin.*size(T1w.Data,1:3);
            temp=zeros(ceil([scale,size(T1w.Data,4)]));
            for d=1:size(T1w.Data,4)
                temp(:,:,:,d)=imresize3(T1w.Data(:,:,:,d),scale);
            end
            T1w.Data=temp;
            T1w.Spc=[spcMin,spcMin,spcMin];
            T1w.Dims=size(T1w.Data,1:3);
            nii=make_nii(T1w.Data,T1w.Spc,T1w.Origin);
        case 'T2w'
            scale=T2w.Spc/spcMin.*size(T2w.Data,1:3);
            T2w.Data=imresize3(T2w.Data,scale);
            T2w.Spc=[spcMin,spcMin,spcMin];
            T2w.Dims=size(T2w.Data,1:3);
            nii=make_nii(T2w.Data,T2w.Spc,T2w.Origin);
        case 'EDDY'
            scale=EDDY.Spc/spcMin.*size(EDDY.Data,1:3);
            EDDY.Data=imresize3(EDDY.Data,scale);
            EDDY.Spc=[spcMin,spcMin,spcMin];
            EDDY.Dims=size(EDDY.Data,1:3);
            nii=make_nii(EDDY.Data,EDDY.Spc,EDDY.Origin);
        case 'DTI'
            scale=DTI.Spc./spcMin.*size(DTI.Data,1:3);
            temp=zeros(ceil([scale,size(DTI.Data,4)]));
            for d=1:size(DTI.Data,4)
                temp(:,:,:,d)=imresize3(DTI.Data(:,:,:,d),scale);
            end
            DTI.b0=imresize3(DTI.b0,scale);
            DTI.Data=temp;
            DTI.Spc=[spcMin,spcMin,spcMin];
            DTI.Dims=size(DTI.Data,1:3);
            nii=make_nii(DTI.Data,DTI.Spc,DTI.Origin);
        case 'NerveVIEW'
            scale=NerveVIEW.Spc/spcMin.*size(NerveVIEW.Data,1:3);
            NerveVIEW.Data=imresize3(NerveVIEW.Data,scale);
            NerveVIEW.Spc=[spcMin,spcMin,spcMin];
            NerveVIEW.Dims=size(NerveVIEW.Data,1:3);
            nii=make_nii(NerveVIEW.Data,NerveVIEW.Spc,NerveVIEW.Origin);
    end
    save([procfilestart '_Rotated.mat'],varnames{i},'-append','-v7.3');
    save_nii(nii,[procfilestart '_' varnames{i} '.nii']);
end
disp('Prepped!')
end