%Quick view of axial, sagittal, and coronal views after prepping GBS data
%to check rotations
%Images
close all; clear; clc;
load('/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Data/GBS_003/Scan 1/Processed/GBS_003_Rotated.mat')

close all;
%Axial
% figure();
% slice=round(size(DTI.b0,3)/2);
% imagesc(DTI.b0(:,:,slice)); colorbar;
% 
% figure();
% slice=round(size(DWI.Data,3)/2);
% imagesc(DWI.Data(:,:,slice,1)); colorbar;

figure();
slice=round(size(MT.sMTOFF,3)/2);
imagesc(MT.sMTOFF(:,:,slice)); colorbar;

figure();
slice=round(size(T2w.Data,3)/2);
imagesc(T2w.Data(:,:,slice)); colorbar;

%Sagittal
% figure();
% y=round(size(DTI.b0,2)/2);
% temp=DTI.b0(:,y,:);
% temp=reshape(temp,size(temp,[1,3]));
% imagesc(temp); colorbar
% 
% figure();
% y=round(size(DWI.Data,2)/2);
% temp=DWI.Data(:,y,:,1);
% temp=reshape(temp,size(temp,[1,3]));
% imagesc(temp); colorbar

figure();
y=round(size(MT.sMTOFF,2)/2);
temp=MT.sMTOFF(:,y,:);
temp=reshape(temp,size(temp,[1,3]));
imagesc(temp); colorbar

figure();
y=round(size(T2w.Data,2)/2);
temp=T2w.Data(:,y,:);
temp=reshape(temp,size(temp,[1,3]));
imagesc(temp); colorbar

%Coronal
% figure();
% x=round(size(DTI.b0,1)/2);
% temp=DTI.b0(x,:,:);
% temp=reshape(temp,size(temp,2:3));
% imagesc(temp); colorbar
% 
% figure();
% x=round(size(DWI.Data,1)/2);
% temp=DWI.Data(x,:,:,1);
% temp=reshape(temp,size(temp,2:3));
% imagesc(temp); colorbar

figure();
x=round(size(MT.sMTOFF,1)/2);
temp=MT.sMTOFF(x,:,:);
temp=reshape(temp,size(temp,2:3));
imagesc(temp); colorbar

figure();
x=round(size(T2w.Data,1)/2);
temp=T2w.Data(x,:,:);
temp=reshape(temp,size(temp,2:3));
imagesc(temp); colorbar

%% Extra scans:
%Axial:
figure();
slice=round(size(DTI.b0,3)/2);
imagesc(DTI.b0(:,:,slice)); colorbar;

%Sagittal:
figure();
y=round(size(DTI.b0,2)/2);
temp=DTI.b0(:,y,:);
temp=reshape(temp,size(temp,[1,3]));
imagesc(temp); colorbar

%Coronal:
figure();
x=round(size(DTI.b0,1)/2);
temp=DTI.b0(x,:,:);
temp=reshape(temp,size(temp,2:3));
imagesc(temp); colorbar

%% %Quick view of axial, sagittal, and coronal views after registering GBS data
close all; clear; clc;
patient='001';
fold=['/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Data/GBS_' patient '/Scan 1/Processed/'];
spm=load([fold 'GBS_' patient '_Registered_SPM.mat']);
vu=load([fold 'GBS_' patient '_Registered_VU.mat']);
vu.reged_MT=vu.Anatomic.Data;
vu.reged_DWI=vu.reged_DWI.Data;
slicer.reged_DI=vuOpenImage([fold 'Output Image Volume.nii']);
slicer.reged_DI=slicer.reged_DI.Data;
slicer.reged_MT=vuOpenImage([fold 'GBS_' patient '_MT.nii']);
slicer.reged_MT=slicer.reged_MT.Data;

imtool3D(vu.reged_DWI)
imtool3D(spm.reged_DWI)
imtool3D(slicer.reged_DI)

