% Yongsheng Chen <ys.chen@wayne.edu>
% 6/3/2022
%
% DTI automatic processing pipeline using DSI Studio
%
% Updated 7/19/2022 by Alison Roth <alison.roth@barrowneuro.org> for use
% with ParRec Files
%
% Download DSI Studio here at:
% https://dsi-studio.labsolver.org/

%%
% clear; close all; clc;
% dirIN='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Scan Development/';
% pat='dev_06';

function DTI=GBS_DTI(dirIN,pat,scan,DTI,DTI_Eddy)

%%
dirPatient=[dirIN 'GBS_' pat '/' scan '/'];
dirRaw=[dirPatient 'Raw/'];
dirOutput=[dirPatient,'Processed/'];
% if 0 == exist(dirOutput,'dir')
%     mkdir(dirOutput) 
% end
strSeqName=['GBS_' pat '_DTI.nii'];
eddyname='DTI_EDDY.nii';
% 
% addpath('/Users/alisonroth/Documents/Code/MATLAB/vuTools/');
% addpath('/Users/alisonroth/Documents/Code/MATLAB/Data_Load_Save');
% 
% img=vuOpenImage([dirOutput strSeqName]);
% dxDWI=img.Spc(1);
% dyDWI=img.Spc(2);
% dzDWI=img.Spc(3);
% origDWI=img.Origin;
% % rowDWIlarge=size(img.Data,1);
% % columnDWIlarge=size(img.Data,2);
% % 
% % rowDWI=img.Parms.scan_resolution(1);
% % columnDWI=img.Parms.scan_resolution(2);
% 
% sDWI=cat(4,img.Data(:,:,:,1:end-2),img.Data(:,:,:,end));

% img2=loadParRec([dirRaw strSeqName]);
% sliceDWI=img2.imgdef.slice_thickness_in_mm.uniq;

% nd=size(DTI.Data,4);
% 
% DTI.b0=img.Data(:,:,:,end);
% 
% fname=[dirOutput, 'BValVec_DTI.txt'];
% if exist(fname)
%     btable=readmatrix(fname);
%     if size(btable,1)~=size(sDWI,4)
%         disp('Error: The B-table from the BValVec_DTI.txt file does not match the dimensions of the DWI data.')
%     end
% else
%     disp('Error: A BValVec_DTI.txt file needs to be created for this patient.')
% end

% nsub=rowDWIlarge/rowDWI;
gyro = 42.57; % kHz/mT

maskDWI=zeros(size(DTI.b0));
maskDWI(DTI.b0>=10)=1;%%%% noise level

mat2analyze_file(DTI.b0,[dirOutput,'GBS_',pat,'_DTI_B0.nii'],DTI.Spc,DTI.Origin,16);

%this flip is for matching the DSIstudio data
mat2analyze_file(flip(maskDWI,2),[dirOutput,'dti_mask_flipped.nii'],DTI.Spc,DTI.Origin,16);
mat2analyze_file(DTI.Data,[dirOutput,'DTI.nii'],DTI.Spc,DTI.Origin,16);

%Eddy Current
if ~isempty(DTI_Eddy)
    mat2analyze_file(DTI_Eddy.Data,[dirOutput,'DTI_EDDY.nii'],DTI.Spc,DTI.Origin,16);
end

%% DSI Studio commandline processing
% dsi_studio='C:\dsi_studio_win\dsi_studio';
dsi_studio='/Applications/dsi_studio.app/Contents/MacOS/dsi_studio';

%convert dicom to src
if contains(dirOutput,'Development')
    dirOutputTerm=['/Users/alisonroth/\Dropbox\ \(Barrow\ Neurological\ Institute\)/Research/GBS/Scan\ \Development/GBS_' pat '/Processed/'];
else
    dirOutputTerm=['/Users/alisonroth/\Dropbox\ \(Barrow\ Neurological\ Institute\)/Research/GBS/Data/GBS_' pat '/Processed/'];
end
strsrc=[' --source=',dirOutputTerm,'DTI.nii'];
strout=[' --output=',dirOutputTerm,'dti.src.gz'];
btab=[' --b_table=',dirOutputTerm,'BValVec_DTI.txt'];
doscmd=[dsi_studio,' --action=src',strsrc,btab,strout];
unix(doscmd);

%dti fitting
strsrc=[' --source=',dirOutputTerm,'dti.src.gz'];
strsrceddy=[' --source=',dirOutputTerm,'DTI_EDDY.nii'];
strmask=[' --mask=',dirOutputTerm,'dti_mask_flipped.nii'];
strmethod=' --method=1';%dti
if exist([dirRaw eddyname])
    doscmd=[dsi_studio,' --action=rec',strsrc,strsrceddy,strmask,strmethod];
else
    doscmd=[dsi_studio,' --action=rec',strsrc,strmask,strmethod];
end
dos(doscmd);

%export maps
strsrc=[' --source=',dirOutputTerm,'dti.src.gz.dti.fib.gz'];
strout=' --export=fa,ad,rd,md';
doscmd=[dsi_studio, ' --action=exp',strsrc,strout];
dos(doscmd);

%% save final dti maps
strfa=gunzip([dirOutput,'dti.src.gz.dti.fib.gz.fa.nii.gz']);
strmd=gunzip([dirOutput,'dti.src.gz.dti.fib.gz.md.nii.gz']);
strad=gunzip([dirOutput,'dti.src.gz.dti.fib.gz.ad.nii.gz']);
strrd=gunzip([dirOutput,'dti.src.gz.dti.fib.gz.rd.nii.gz']);

%flip back for the final results
% fa=flip(read_analyze(char(strfa)),2);
% md=flip(read_analyze(char(strmd)),2);
% ad=flip(read_analyze(char(strad)),2);
% rd=flip(read_analyze(char(strrd)),2);
fa=load_nii([dirOutput,'dti.src.gz.dti.fib.gz.fa.nii.gz']);
md=load_nii([dirOutput,'dti.src.gz.dti.fib.gz.md.nii.gz']);
ad=load_nii([dirOutput,'dti.src.gz.dti.fib.gz.ad.nii.gz']);
rd=load_nii([dirOutput,'dti.src.gz.dti.fib.gz.rd.nii.gz']);

DTI.sFA=fa.img;
DTI.sMD=md.img;
DTI.sAD=ad.img;
DTI.sRD=rd.img;

mat2analyze_file(fa.img.*maskDWI*1000,[dirOutput,'GBS_' pat '_DTI_FA.nii'],DTI.Spc,DTI.Origin,16);
mat2analyze_file(md.img.*maskDWI*1000,[dirOutput,'GBS_' pat '_DTI_MD.nii'],DTI.Spc,DTI.Origin,16);
mat2analyze_file(ad.img.*maskDWI*1000,[dirOutput,'GBS_' pat '_DTI_AD.nii'],DTI.Spc,DTI.Origin,16);
mat2analyze_file(rd.img.*maskDWI*1000,[dirOutput,'GBS_' pat '_DTI_RD.nii'],DTI.Spc,DTI.Origin,16);

%delete temporary files
delete([dirOutput,'dti*.*']);

end
