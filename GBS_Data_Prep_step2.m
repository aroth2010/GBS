%This folder combines the final .mat files from GBS_Data_Prep into a
%single .mat file for processing

% close all; clear; clc
% patient='001'; %'dev_02' or for patients: '001'
% FolderLoc='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Scan Development/';
% FolderLoc='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Data/';

function GBS_Data_Prep_step2(patient, FolderLoc)

procFolder=[FolderLoc 'GBS_' patient '/Processed/'];
scans=dir([procFolder '*.mat']);
savefilename=[procFolder '/GBS_' patient '.mat'];

for s=1:size(scans,1)
    load([procFolder scans(s).name])
    
    if strfind(scans(s).name,['GBS_' patient '_MTR'])
        if exist(savefilename)
            save(savefilename, 'dxMT', 'dyMT', 'dzMT', 'origMT', 'MTTR', 'sMTON', 'sMTOFF','MTOFFTEs','MTFA','-append');
        else
            save(savefilename, 'dxMT', 'dyMT', 'dzMT', 'origMT', 'sMTON', 'sMTOFF', 'MTOFFTEs','MTTR','MTFA');
        end
        delete([procFolder scans(s).name])
        
    elseif strfind(scans(s).name,['GBS_' patient '_AFI'])
        
        if exist(savefilename)
            save(savefilename, 'dxAFI', 'dyAFI', 'dzAFI', 'origAFI', 'sAFI', 'AFITRs', 'AFIFA','-append');
        else
            save(savefilename, 'dxAFI', 'dyAFI', 'dzAFI', 'origAFI', 'sAFI', 'AFITRs', 'AFIFA');
        end
        delete([procFolder scans(s).name])
        
    elseif strfind(scans(s).name,['GBS_' patient '_DWI'])
        if exist(savefilename)
            save(savefilename, 'bDWI', 'dxDWI', 'dyDWI', 'dzDWI', 'origDWI', 'sDWI', 'DWITRs', 'DWIFA','-append');
        else
            save(savefilename, 'bDWI', 'dxDWI', 'dyDWI', 'dzDWI', 'origDWI', 'sDWI', 'DWITRs', 'DWIFA');
        end
        delete([procFolder scans(s).name])
        
    elseif strfind(scans(s).name,['GBS_' patient '_PDw'])
        if exist(savefilename)
            save(savefilename, 'dxPDw', 'dyPDw', 'dzPDw', 'origPDw', 'sPDw', 'PDwTR', 'PDwFA','-append');
        else
            save(savefilename, 'dxPDw', 'dyPDw', 'dzPDw', 'origPDw', 'sPDw', 'PDwTR', 'PDwFA');
        end
        delete([procFolder scans(s).name])
        
    elseif strfind(scans(s).name,['GBS_' patient '_T1w'])
        if exist(savefilename)
            save(savefilename, 'dxT1w', 'dyT1w', 'dzT1w', 'origT1w', 'sT1w', 'T1wTR', 'T1wFA','-append');
        else
            save(savefilename, 'dxT1w', 'dyT1w', 'dzT1w', 'origT12', 'sT1w', 'T1wTR', 'T1wFA');
        end
        delete([procFolder scans(s).name])
        
    elseif strfind(scans(s).name,['GBS_' patient '_T2w'])
        if exist(savefilename)
            save(savefilename, 'dxT2w', 'dyT2w', 'dzT2w', 'origT2w', 'sT2w', 'T2wTR', 'T2wFA','-append');
        else
            save(savefilename, 'dxT2w', 'dyT2w', 'dzT2w', 'origT2w', 'sT2w', 'T2wTR', 'T2wFA');
        end
        delete([procFolder scans(s).name])

    elseif strfind(scans(s).name,['GBS_' patient '_DTI_EDDY'])
        if exist(savefilename)
            save(savefilename, 'sDTI_EDDY','-append');
        else
            save(savefilename, 'sDTI_EDDY');
        end
        delete([procFolder scans(s).name])
        
    elseif strfind(scans(s).name,['GBS_' patient '_DTI'])
        if exist(savefilename)
            save(savefilename, 'dxDTI', 'dyDTI', 'dzDTI', 'origDTI', 'sDTI', 'DTITR', 'DTIFA','DTIb','DTIdir','-append');
        else
            save(savefilename, 'dxDTI', 'dyDTI', 'dzDTI', 'origDTI', 'sDTI', 'DTITR', 'DTIFA','DTIb','DTIdir');
        end
        delete([procFolder scans(s).name])
        
    elseif strfind(scans(s).name,['GBS_' patient '_3D_GRE'])
        if exist(savefilename)
            save(savefilename, 'dxGRE', 'dyGRE', 'dzGRE', 'origGRE', 'sGRE', 'GRETR', 'GREFA','-append');
        else
            save(savefilename, 'dxGRE', 'dyGRE', 'dzGRE', 'origGRE', 'sGRE', 'GRETR', 'GREFA');
        end
        delete([procFolder scans(s).name])
        
%     elseif strfind(scans(s).name,['GBS_' patient '_Dixon'])
%         if exist(savefilename)
%             save(savefilename, 'dxmDixon', 'dymDixon', 'dzmDixon', 'smDixon', 'mDixonTR', 'mDixonFA', 'mDixonTEs','-append');
%         else
%             save(savefilename, 'dxmDixon', 'dymDixon', 'dzmDixon', 'smDixon', 'mDixonTR', 'mDixonFA', 'mDixonTEs');
%         end
%         delete([procFolder scans(s).name])
        
    elseif strfind(scans(s).name,['GBS_' patient '_nerveVIEW'])
        if exist(savefilename)
            save(savefilename, 'dxnerveVIEW', 'dynerveVIEW', 'dznerveVIEW', 'orignerveVIEW', 'snerveVIEW', 'nerveVIEWTR', 'nerveVIEWFA','-append');
        else
            save(savefilename, 'dxnerveVIEW', 'dynerveVIEW', 'dznerveVIEW', 'orignerveVIEW', 'snerveVIEW', 'nerveVIEWTR', 'nerveVIEWFA');
        end
        delete([procFolder scans(s).name])
    end
    clearvars -except patient FolderLoc procFolder scans s savefilename
end
end