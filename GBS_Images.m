%GBSImages
close all; clear; clc;
pat={'dev_01','dev_02','001','002','003','003'}; 
scans={'Scan 1','Scan 1','Scan 1','Scan 1','Scan 1','Scan 2'};

addpath('/Users/alisonroth/Documents/Code/MATLAB/Data_Load_Save')
load('/Users/alisonroth/Documents/Code/MATLAB/colormaps/jetb.mat')
saveloc='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Analysis/Plots/Images/';

for p=1:1%size(pat,2)
    subject=pat{p};
    scan=scans{p};
    disp([subject ' ' scan]);

    switch strcmp(subject(1),'d')
        case 0
            fold='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Data/';
        case 1
            fold='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Scan Development/';
    end
    fold=[fold 'GBS_' subject '/' scan '/Processed/'];
    load([fold 'GBS_' subject '_Registered.mat'])
    roi=load_nii([fold 'GBS_' subject '_Segmentation.nii']);
    roi=single(roi.img);
    
    % %Image dimensions
    ind=find(roi);
    [x,y,z]=ind2sub(size(roi),ind);
    z=unique(z);
    slice=round(median(z));
    if size(roi,1)>size(roi,2)
        pad1=0;
        pad2=size(roi,1)-size(roi,2);
    elseif size(roi,1)<size(roi,2)
        pad1=size(roi,2)-size(roi,1);
        pad2=0;
    else
        pad1=0;
        pad2=0;
    end

    %Anatomic image
    temp=padarray(reged_MTOFF.Data(:,:,slice),[pad1, pad2],0,'pre');
    mnmx=[min(temp(:)),0.8*max(temp(:))];
    figure('Position',[10000,10000,900,900])
    A=imagesc(temp,mnmx); colormap('gray'); xticklabels(''); yticklabels('')
    set(gca,'LooseInset',get(gca,'TightInset'));
    fname=[saveloc subject '_' scan '_Anatomic.png'];
%     saveas(gcf,fname);

    %MTw image
    temp=padarray(reged_MTON.Data(:,:,slice),[pad1, pad2],0,'pre');
    figure('Position',[10000,10000,900,900])
    A=imagesc(temp,mnmx); colormap('gray'); xticklabels(''); yticklabels('')
    set(gca,'LooseInset',get(gca,'TightInset'));
    fname=[saveloc subject '_' scan '_MTw.png'];
%     saveas(gcf,fname);
    
    %MTR
    temp=mtr;%.*roi;
    temp=padarray(temp(:,:,slice),[pad1, pad2],0,'pre');
    figure('Position',[10000,10000,900,900])
    B=imagesc(temp,[0,10]); colormap(gca,jetb); xticklabels(''); yticklabels(''); 
    set(gca,'LooseInset',get(gca,'TightInset'));
    fname=[saveloc subject '_' scan '_MTRuc_noroi.png'];
    saveas(gcf,fname);
%     colorbar
%     fname=[saveloc subject '_' scan '_MTRuc_colorbar.png'];
%     saveas(gcf,fname);
    
    %MTsat
    if exist('mtsat','var')
        temp=mtsat;%.*roi;
        temp=padarray(temp(:,:,slice),[pad1, pad2],0,'pre');
        figure('Position',[10000,10000,900,900])
        imagesc(temp,[0,2]), colormap(gca,jetb), xticklabels(''); yticklabels(''); 
        set(gca,'LooseInset',get(gca,'TightInset'));
        fname=[saveloc subject '_' scan '_MTsat_noroi.png'];
        saveas(gcf,fname);
%         colorbar
%         fname=[saveloc subject '_' scan '_MTsat_colorbar.png'];
%         saveas(gcf,fname);
    end
    clear pad1 pad2 mtr mtsat reged_MTON reged_MTOFF roi
end
close all

%Sagittal view:
% % %Image dimensions
% ind=find(roi);
% [x,y,z]=ind2sub(size(roi),ind);
% slice=median(y);
% 
% %Anatomic image
% temp=reged_MTOFF.Data(:,slice,:);
% temp=reshape(temp,size(temp,1,3));
% figure('Position',[10000,10000,900,900])
% A=imagesc(temp); colormap('gray'); xticklabels(''); yticklabels('')
% set(gca,'LooseInset',get(gca,'TightInset'));
% fname=[saveloc subject '_' scan '_Anatomic.png'];
% saveas(gcf,fname);
% 
% %MTR
% temp=mtr.*roi;
% temp=temp(:,slice,:);
% temp=reshape(temp,size(temp,1,3));
% figure('Position',[10000,10000,900,900])
% B=imagesc(temp,[0,10]); colormap(gca,jetb); xticklabels(''); yticklabels(''); 
% set(gca,'LooseInset',get(gca,'TightInset'));
% fname=[saveloc subject '_' scan '_MTRuc.png'];
% saveas(gcf,fname);
% colorbar
% fname=[saveloc subject '_' scan '_MTRuc_colorbar.png'];
% saveas(gcf,fname);
% 
% %MTsat
% temp=mtsat.*roi;
% temp=temp(:,slice,:);
% temp=reshape(temp,size(temp,1,3));
% figure('Position',[10000,10000,900,900])
% imagesc(temp,[0,2]), colormap(gca,jetb), xticklabels(''); yticklabels(''); 
% set(gca,'LooseInset',get(gca,'TightInset'));
% fname=[saveloc subject '_' scan '_MTsat.png'];
% saveas(gcf,fname);
% colorbar
% fname=[saveloc subject '_' scan '_MTsat_colorbar.png'];
% saveas(gcf,fname);