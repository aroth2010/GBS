%This is the final step in the GBS data processing pipeline
%
% Prior to running this script, run GBS_Data_Prep.m and GBS_Data_Processing.m
% 
% This script finds the mean map values (ADC, AD, RD, MD, FA, MTR, MTRc, MTsat) 
% for each segmented structure

% close all; clear; clc
% patient='dev_01';
% FolderLoc='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Scan Development/';
% FolderLoc='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Data/';

function out=GBS_Data_Analysis(patient,FolderLoc,scan)
procFolder=[FolderLoc 'GBS_' patient '/' scan '/Processed/'];
procfilestart=[procFolder patient '_'];

load([procFolder '/GBS_' patient '_Registered.mat']);
try ROI=load_nii([procFolder '/GBS_' patient '_Segmentation.nii']);
catch ROI=load_nii([procFolder '/GBS_' patient '_Segmentation.nii.gz']);
end
ROI=ROI.img;

segs=unique(ROI(:));
segs=segs(2:end);
out=NaN(1,9*size(segs,1));
for s=segs'
    ind=find(ROI==s);
    if exist('reged_ADC','var')
        out(1,9*(s-1)+1)=mean(reged_ADC.Data(ind)); %Mean ADC
    end
    if exist('reged_ADmap','var')
        out(1,9*(s-1)+2)=mean(reged_ADmap.Data(ind)); %Mean AD
        out(1,9*(s-1)+3)=mean(reged_RDmap.Data(ind)); %Mean RD
        out(1,9*(s-1)+4)=mean(reged_MDmap.Data(ind)); %Mean MD/ADC
        out(1,9*(s-1)+5)=mean(reged_FAmap.Data(ind)); %Mean FA
    end
    out(1,9*(s-1)+6)=mean(mtr(ind)); %Mean MTR
    if exist('mtrc','var')
        out(1,9*(s-1)+7)=mean(mtrc(ind)); %Mean MTRc
    end
    if exist('mtsat','var')
        out(1,9*(s-1)+8)=mean(mtsat(ind)); %Mean MTsat
    end
    out(1,9*(s-1)+9)=mean(reged_MTON.Data(ind)/mean(reged_MTOFF.Data(ind)));
end

end