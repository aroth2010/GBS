%This is the second step in the GBS data processing pipeline
%
% Prior to running this script, run GBS_Data_Prep.m
% 
% This script creates maps (AFI B1, MT, DTI, DWI, etc.), registers images,
% and creates GBS_X_Registered.mat

% close all; clear; clc
% patient='phantom_02';
% scan='Scan 1';
% Registration=1; % 0 for no-coregistration, 1 for co-registration
% FolderLoc='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Scan Development/';
% FolderLoc='/Users/alisonroth/Dropbox (Barrow Neurological Institute)/Research/GBS/Data/';

function GBS_Data_Processing(patient,FolderLoc,scan,Registration)

addpath('/Users/alisonroth/Documents/Code/MATLAB/vuTools/');
addpath('/Users/alisonroth/Documents/Code/MATLAB/spm12/')
% addpath('/Users/alisonroth/Documents/Code/MATLAB/Data_Load_Save/')

procFolder=[FolderLoc 'GBS_' patient '/' scan '/Processed/'];
procfilestart=[procFolder '/GBS_' patient];

load([procFolder '/GBS_' patient '_Rotated.mat'])

%% AFI B1 mapping
if exist('B1_AFI','var')
    minB1=0.3;
    maxB1=2.0;
    r=B1_AFI.Data(:,:,:,2)./B1_AFI.Data(:,:,:,1);
    n=B1_AFI.TR(2)/B1_AFI.TR(1);
    aFA=abs(acos((n.*r-1)./(n-r)));
    B1_AFI.AFIB1map=aFA./(B1_AFI.FA.*pi./180);
    B1_AFI.AFIB1map(isnan(B1_AFI.AFIB1map))=0;
    B1_AFI.AFIB1map(B1_AFI.AFIB1map<minB1)=0;
    B1_AFI.AFIB1map(B1_AFI.AFIB1map>maxB1)=0;
end

%% DWI processing
if exist('DWI','var')
    DWI.sADC=-1/DWI.bDWI*log(DWI.Data(:,:,:,1)./(DWI.Data(:,:,:,2)));
    DWI.sADC(isnan(DWI.sADC))=0;
    DWI.sADC(isinf(DWI.sADC))=0;
%     figure();
%     imagesc(DWI.sADC(:,:,5)); colorbar
    DWI.Data=DWI.Data(:,:,:,2);
end

%% image registration %SPM12 Registration
%Registration prep: no coregistration, Original data
if exist('MT','var')
    reged_MTON=MT;
    reged_MTON.Data=MT.sMTON;

    reged_MTOFF=MT;
    reged_MTOFF.Data=MT.sMTOFF;
end

if exist('B1_AFI','var')
    reged_B1map=B1_AFI;
    reged_B1map.Data=B1_AFI.AFIB1map;
end

if exist('T2w','var')
    reged_T2w=T2w;
end

if exist('PDw','var')
    reged_PDw=PDw;
end

if exist('T1w','var')
    reged_T1w=T1w;
end

if exist('DWI','var')
    reged_DWI=DWI;
    reged_ADCmap=DWI;
    reged_ADCmap.Data=DWI.sADC;
end

if exist('DTI','var')
    reged_DTI=DTI;
    if exist('DTI_Eddy','var')
        reged_DTI_Eddy=DTI_Eddy;
    end

    DTItemp=DTI;
    DTItemp=rmfield(DTItemp,{'Data'});

    reged_ADmap=DTItemp;
    reged_RDmap=DTItemp;
    reged_MDmap=DTItemp;
    reged_FAmap=DTItemp;
end

if Registration >0 %vuAffineRegistration
    mat2analyze_file(reged_MTOFF.Data,[procFolder,'MTOFF.nii'],[reged_MTOFF.Spc],[0,0,0],16);
    prefix='regToMTOFF_';

    if exist('MT','var')
        disp('MT')
        MT.Data=MT.sMTON;
        mat2analyze_file(MT.Data,[procFolder,'MTON.nii'],[MT.Spc],[0,0,0],16);

        ref_nii_path={[procFolder,'MTOFF.nii',',1']};
        mov_nii_path={[procFolder,'MTON.nii',',1']};
        other_nii_path={[]};
        SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

        temp=vuOpenImage([procFolder,prefix,'MTON.nii']);
        reged_MTON=catstruct(reged_MTON,temp);
        if max(size(reged_MTON.Data)~=reged_MTON.Dims)
            reged_MTON.Data=permute(reged_MTON.Data,[2, 1, 3]);
        end
    end
    
    if exist('B1_AFI','var')
        disp('AFI')
        mat2analyze_file(B1_AFI.Data,[procFolder,'B1_AFI.nii'],[B1_AFI.Spc],[0,0,0],16);
        mat2analyze_file(B1_AFI.AFIB1map,[procFolder,'AFI_B1map.nii'],[B1_AFI.Spc],[0,0,0],16);

        ref_nii_path={[procFolder,'MTOFF.nii',',1']};
        mov_nii_path={[procFolder,'B1_AFI.nii',',1']};
        other_nii_path={[procFolder,'AFI_B1map.nii',',1']};
        SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

        temp=vuOpenImage([procFolder,prefix,'B1_AFI.nii']);
        reged_B1map=catstruct(reged_B1map,temp);
        if max(size(reged_B1map.Data)~=reged_B1map.Dims)
            reged_B1map.Data=permute(reged_B1map.Data,[2, 1, 3]);
        end
        temp=vuOpenImage([procFolder,prefix,'AFI_B1map.nii']);
        if max(size(temp.Data)~=reged_B1map.Dims)
            reged_B1map.AFIB1map=permute(temp.Data,[2, 1, 3]);
        else
            reged_B1map.AFIB1map=temp.Data;
        end
    end
    
    if exist('T2w','var')
        disp('T2w')
        mat2analyze_file(T2w.Data,[procFolder,'T2w.nii'],[T2w.Spc],[0,0,0],16);

        ref_nii_path={[procFolder,'MTOFF.nii',',1']};
        mov_nii_path={[procFolder,'T2w.nii',',1']};
        other_nii_path={[]};
        SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

        temp=vuOpenImage([procFolder,prefix,'T2w.nii']);
        reged_T2w=catstruct(reged_T2w,temp);
        if max(size(reged_T2w.Data)~=reged_T2w.Dims)
            reged_T2w.Data=permute(reged_T2w.Data,[2, 1, 3]);
        end
    end
    
    if exist('PDw','var')
        disp('PDw')
        mat2analyze_file(PDw.Data,[procFolder,'PDw.nii'],[PDw.Spc],[0,0,0],16);

        ref_nii_path={[procFolder,'MTOFF.nii',',1']};
        mov_nii_path={[procFolder,'PDw.nii',',1']};
        other_nii_path={[]};
        SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

        temp=vuOpenImage([procFolder,prefix,'PDw.nii']);
        reged_PDw=catstruct(reged_PDw,temp);
        if max(size(reged_PDw.Data)~=reged_PDw.Dims)
            reged_PDw.Data=permute(reged_PDw.Data,[2, 1, 3]);
        end
    end
    
    if exist('T1w','var')
        disp('T1w')
        mat2analyze_file(T1w.Data,[procFolder,'T1w.nii'],[T1w.Spc],[0,0,0],16);

        ref_nii_path={[procFolder,'MTOFF.nii',',1']};
        mov_nii_path={[procFolder,'T1w.nii',',1']};
        other_nii_path={[]};
        SPM_registration(prefix,ref_nii_path,mov_nii_path,other_nii_path)

        temp=vuOpenImage([procFolder,prefix,'T1w.nii']);
        reged_T1w=catstruct(reged_T1w,temp);
        if max(size(reged_T1w.Data)~=reged_T1w.Dims)
            reged_T1w.Data=permute(reged_T1w.Data,[2, 1, 3]);
        end
    end
end

if exist('MT','var')
    reged_MTON.Data(isnan(reged_MTON.Data))=0;
    reged_MTOFF.Data(isnan(reged_MTOFF.Data))=0;

    try reged_MTON=rmfield(reged_MTON,{'sMTON','sMTOFF'}); end
    reged_MTOFF=rmfield(reged_MTOFF,{'sMTON','sMTOFF'});
end

if exist('B1_AFI','var')
    reged_B1map.Data(isnan(reged_B1map.Data))=0;
    try reged_B1map=rmfield(reged_B1map,'AFIB1map'); end
end

if exist('T2w','var')
    reged_T2w.Data(isnan(reged_T2w.Data))=0;
end

if exist('PDw','var')
    reged_PDw.Data(isnan(reged_PDw.Data))=0;
end

if exist('T1w','var')
    reged_T1w.Data(isnan(reged_T1w.Data))=0;
end

if exist('DWI','var')
    reged_DWI.Data(isnan(reged_DWI.Data))=0;
    reged_ADCmap.Data(isnan(reged_ADCmap.Data))=0;
    try reged_ADCmap=rmfield(reged_ADCmap,'sADC'); end
end

if exist('DTI','var')
    reged_DTI.Data(isnan(reged_DTI.Data))=0;
end

delete([procFolder '*.nii'])
disp('Registration finished');

%% DTI processing
if exist('DTI','var')
    if exist('DTI_Eddy','var')
        reged_DTI=GBS_DTI(FolderLoc,patient,scan,reged_DTI,reged_DTI_Eddy);
    else
        reged_DTI=GBS_DTI(FolderLoc,patient,scan,reged_DTI,[]);
    end
    reged_ADmap.Data=reged_DTI.sAD;
    reged_RDmap.Data=reged_DTI.sRD;
    reged_MDmap.Data=reged_DTI.sMD;
    reged_FAmap.Data=reged_DTI.sFA;
end

%% MTR
mtr=100*(1-reged_MTON.Data./reged_MTOFF.Data);
mtr(isnan(mtr))=0;
mtr(mtr>100)=100;
mtr(mtr<0)=0;
mtr(isnan(mtr))=0;

save([procfilestart '_Registered.mat'],'mtr','-v7.3');
mat2analyze_file(mtr,[procfilestart,'_final_MTR.nii'],MT.Spc,MT.Origin,16);

% MTR correction using AFI
if exist('B1_AFI','var')
    Mask_fatsat=ones(size(reged_MTOFF));
    Mask_fatsat(reged_MTOFF.Data/max(reged_MTOFF.Data(:))<0.35)=0;% mask removing noise region

    B1Err=(reged_B1map.Data-B1_AFI.FA*pi/180)/(B1_AFI.FA*pi/180); %Check that afi_B1map isn't >1000
    p=polyfit(mtr(Mask_fatsat>0),B1Err(Mask_fatsat>0),1);
    k=p(1)/p(2);
    mtrc=mtr./(1+k*B1Err);
    
    %mtrc=mtrc.*Mask_fatsat;
    mtrc(isnan(mtrc))=0;
    mtrc(mtrc<0)=0;
    mtrc(mtrc>100)=100;
    
    save([procfilestart '_Registered.mat'],'mtrc','-append','-v7.3');
    mat2analyze_file(mtrc,[procfilestart,'_final_MTRc.nii'],MT.Spc,MT.Origin,16);
end

%MTsat calculation
if exist('T1w','var') && exist('MT','var')
    if reged_MTOFF.FA>pi
        reged_MTOFF.FA=reged_MTOFF.FA*pi/180;
    end
    if reged_T1w.FA>pi
        reged_T1w.FA=reged_T1w.FA*pi/180;
    end
    
    A=(reged_MTOFF.TR*reged_T1w.FA/reged_MTOFF.FA - reged_T1w.TR*reged_MTOFF.FA/reged_T1w.FA).*(reged_MTOFF.Data.*reged_T1w.Data./(reged_MTOFF.TR*reged_T1w.FA.*reged_T1w.Data-reged_T1w.TR*reged_MTOFF.FA.*reged_MTOFF.Data));
    R1=0.5*(reged_T1w.Data*reged_T1w.FA/reged_T1w.TR - reged_MTOFF.Data*reged_MTOFF.FA/reged_MTOFF.TR)./(reged_MTOFF.Data/reged_MTOFF.FA-reged_T1w.Data/reged_T1w.FA);
    mtsat=100*((A*reged_MTOFF.FA./reged_MTON.Data-1).*R1*reged_MTOFF.TR - ((reged_MTOFF.FA)^2)/2);

    %mtsat=mtsat.*Mask_fatsat;
    mtsat(isnan(mtsat))=0;
    mtsat(mtsat<0)=0;
    mtsat(mtsat>100)=100;
    
    save([procfilestart '_Registered.mat'],'mtsat','-append','-v7.3');
    mat2analyze_file(mtsat,[procfilestart,'_final_MTsat.nii'],MT.Spc,MT.Origin,16);
end

%% save other files
if exist('B1_AFI','var')
    save([procfilestart '_Registered.mat'],'reged_B1map','-append','-v7.3');
%     mat2analyze_file(reged_AFI.Data,[procfilestart,'_final_AFI.nii'],AFI.Spc,AFI.Origin,16);
    mat2analyze_file(reged_B1map.Data,[procfilestart,'_final_AFI_B1map.nii'],B1_AFI.Spc,B1_AFI.Origin,16);
end
if exist('T2w','var')
    save([procfilestart '_Registered.mat'],'reged_T2w','-append','-v7.3');
    mat2analyze_file(reged_T2w.Data,[procfilestart,'_final_T2w.nii'],T2w.Spc,T2w.Origin,16);
end
if exist('PDw','var')
    save([procfilestart '_Registered.mat'],'reged_PDw','-append','-v7.3');
    mat2analyze_file(reged_PDw.Data,[procfilestart,'_final_PDw.nii'],PDw.Spc,PDw.Origin,16);
end
if exist('T1w','var')
    save([procfilestart '_Registered.mat'],'reged_T1w','-append','-v7.3');
    mat2analyze_file(reged_T1w.Data,[procfilestart,'_final_T1w.nii'],T1w.Spc,T1w.Origin,16);
end
if exist('DWI','var')
    save([procfilestart '_Registered.mat'],'reged_ADCmap','-append','-v7.3');
    mat2analyze_file(reged_DWI.Data,[procfilestart,'_final_DWI.nii'],DWI.Spc,DWI.Origin,16);
    mat2analyze_file(reged_ADCmap.Data,[procfilestart,'_final_ADCmap.nii'],DWI.Spc,DWI.Origin,16);
end

if exist('DTI','var')
    save([procfilestart '_Registered.mat'],'reged_DTI','reged_ADmap','reged_RDmap','reged_MDmap','reged_FAmap','-append','-v7.3');
    mat2analyze_file(reged_DTI.Data,[procfilestart,'_final_DTI.nii'],DTI.Spc,DTI.Origin,16);
    mat2analyze_file(reged_ADmap.Data,[procfilestart,'_final_DTI_ADmap.nii'],DTI.Spc,DTI.Origin,16);
    mat2analyze_file(reged_RDmap.Data,[procfilestart,'_final_DTI_RDmap.nii'],DTI.Spc,DTI.Origin,16);
    mat2analyze_file(reged_MDmap.Data,[procfilestart,'_final_DTI_MDmap.nii'],DTI.Spc,DTI.Origin,16);
end

save([procfilestart '_Registered.mat'],'reged_MTOFF','-append','-v7.3');
mat2analyze_file(reged_MTOFF.Data,[procfilestart,'_final_MTOFF.nii'],MT.Spc,MT.Origin,16);
save([procfilestart '_Registered.mat'],'reged_MTON','-append','-v7.3');
mat2analyze_file(reged_MTON.Data,[procfilestart,'_final_MTON.nii'],MT.Spc,MT.Origin,16);
disp([patient ' Processed!'])
end